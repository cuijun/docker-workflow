package middleware

import (
	"devOpsApi/libs"
	"github.com/kataras/iris/context"
)

//缓存中间件 拦截路由 检测是否存在缓存key
func CacheServe(ctx context.Context) {
	cacheKey := libs.GetCacheKey(ctx)
	value, found := libs.GetCache(cacheKey)
	if found {
		ctx.JSON(value)
		return
	}
	ctx.Next()
}
