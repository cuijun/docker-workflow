package base

const (
	// about sso
	SSO_URL_LOGIN_TOKEN      string = `/oauth/token`   //  login
	SSO_URl_GETINFO_BY_TOKEN string = `/oauth/idToken` // get info by idToken/access_token

	SSO_URL_USERINFO string = `/oauth/idToken` // get user info by access_token
)

const (
	BatchOptDel       int = 1 // 批量删除
	BatchOptUpdStatus int = 2 // 批量修改状态
	BatchOptUpdLevel  int = 3 // 批量修改优先级
	BatchOptUpdStep   int = 4 // 批量修改阶段
)

func GetStatusStr(index int) string {
	if index > 0 && index < 5 {
		return []string{"", "待评审", "修改中", "已评审", "已驳回"}[index]
	}
	return ""
}

func GetLevelStr(index int) string {
	if index > 0 && index < 5 {
		return []string{"", "较低", "普通", "较高", "紧急"}[index]
	}
	return ""
}
