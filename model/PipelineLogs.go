package model

import (
	"devOpsApi/common/util"
	"devOpsApi/libs"
	"github.com/jinzhu/gorm"
	"math"
)

type PipelineLogs struct {
	gorm.Model
	db            *gorm.DB
	UserId        string `gorm:"column:userId" json:"userId"`
	RealName      string `gorm:"column:realName" json:"realName"`
	ErrMsg        string `gorm:"column:errMsg" json:"errMsg"`
	Coderepo      string `gorm:"column:codeRepo" json:"codeRepo"`
	ZipName       string `gorm:"column:zipName" json:"zipName"`
	ZipPath       string `gorm:"column:zipPath" json:"zipPath"`
	Branch        string `gorm:"column:branch" json:"branch"`
	GitCommit     string `gorm:"column:gitCommit" json:"gitCommit"`
	Cate          uint   `gorm:"column:cate" json:"cate" form:"cate"`
	Speed         string `gorm:"column:speed" json:"speed" form:"speed"`
	Monitor       string `gorm:"column:monitor" json:"monitor" form:"monitor"`
	Pid           uint   `gorm:"column:pid" json:"pid" form:"pid"`
	Message       string `gorm:"column:message" json:"message" form:"message"`
	ErrorPosition string `gorm:"column:error_position" json:"error_position" form:"error_position"`
}

func (PipelineLogs) TableName() string {
	return "pipeline_logs"
}

func NewPipelineLogs() *PipelineLogs {
	return &PipelineLogs{db: libs.DB}
}

func (m *PipelineLogs) List(pid, page, pagesize uint, searchMap ...map[string]string) ([]PipelineLogs, uint, uint) {
	var data = []PipelineLogs{}
	var totalCount uint
	limit := pagesize
	offset := (page - 1) * limit
	db := m.db.Model(&PipelineLogs{})
	db = db.Where("pid=? ", pid)
	if len(searchMap) > 0 {
		for k, v := range searchMap[0] {
			if k != "" && v != "" {
				db = db.Where(k+" =  ? ", v)
			}
		}
	}
	db.Count(&totalCount)
	err := db.Select("id,userId,realName,errMsg,codeRepo,zipName,zipPath,branch,gitCommit,cate,pid,monitor,error_position,speed,created_at,updated_at").Offset(offset).Limit(limit).Order("id desc").Find(&data).Error
	if err != nil {
		//log.Fatalln(err)
	}
	totalPages := uint(math.Ceil(float64(totalCount) / float64(limit)))
	return data, totalCount, totalPages
}

func (m *PipelineLogs) Create() (insertID uint, err error) {
	if err := util.Validate(m); err != nil {
		return m.ID, err
	}
	err = m.db.Create(m).Error
	if err != nil {
		return m.ID, err
	}
	return m.ID, nil
}

func (m *PipelineLogs) UpdateFields(id uint, fields map[string]interface{}) error {
	return m.db.Model(&PipelineLogs{}).Where("id = ?", id).Updates(fields).Error
}

func (m *PipelineLogs) Del(id uint) error {
	return m.db.Where("id = ?", id).Delete(m).Error
}

func (m *PipelineLogs) DelByPid(pid uint) error {
	return m.db.Where("pid = ?", pid).Delete(m).Error
}

func (m *PipelineLogs) Detail(id uint) (pipelineLogs PipelineLogs, err error) {
	err = m.db.Where("id = ?", id).First(&pipelineLogs).Error
	return
}

func (m *PipelineLogs) Count(id uint) uint {
	var totalCount uint
	m.db.Model(&PipelineLogs{}).Where("pid = ? and monitor != ?", id, "import").Count(&totalCount)
	return totalCount
}

func (m *PipelineLogs) Last() (PipelineLogs, error) {
	var pipelineLogs PipelineLogs
	m.db.Last(&pipelineLogs)
	return pipelineLogs, nil
}
