package model

import (
	"devOpsApi/common/util"
	"devOpsApi/libs"
	"github.com/jinzhu/gorm"
	"math"
)

type PipelineFile struct {
	gorm.Model
	db        *gorm.DB
	Uid       uint   `gorm:"column:uid" json:"uid" form:"uid" validate:"required"`
	Pid       uint   `gorm:"column:pid" json:"pid" form:"pid"`
	LocalFile string `gorm:"column:local_file" json:"local_file" form:"local_file" validate:"required"`
	ShortName string `gorm:"column:short_name" json:"short_name" form:"short_name" validate:"required"`
	Md5       string `gorm:"column:md5" json:"md5" form:"md5" validate:"required"`
}

type PipelineFileList struct {
	gorm.Model
	db        *gorm.DB
	Uid       uint   `gorm:"column:uid" json:"uid" form:"uid" validate:"required"`
	Pid       uint   `gorm:"column:pid" json:"pid" form:"pid"`
	LocalFile string `gorm:"column:local_file" json:"local_file" form:"local_file" validate:"required"`
	ShortName string `gorm:"column:short_name" json:"short_name" form:"short_name" validate:"required"`
	Md5       string `gorm:"column:md5" json:"md5" form:"md5" validate:"required"`
	RealName  string `gorm:"column:realName" json:"RealName,omitempty"`
	Avatar    string `gorm:"avatar" json:"Avatar,omitempty"`
}

func (PipelineFile) TableName() string {
	return "pipeline_file"
}

func NewPipelineFile() *PipelineFile {
	return &PipelineFile{db: libs.DB}
}

func (m *PipelineFile) List(uid, page, pagesize uint, title string) ([]PipelineFileList, uint, uint) {
	var data = []PipelineFileList{}
	var totalCount uint
	db := libs.DB
	limit := pagesize
	offset := (page - 1) * limit
	if title != "" {
		db = db.Where("short_name LIKE ? ", "%"+title+"%")
	}
	db = m.adminCheck(uid, db)
	db.Table("pipeline_file").Count(&totalCount)
	err := db.Table("pipeline_file").Select("pipeline_file.id,pipeline_file.uid,pipeline_file.pid,pipeline_file.short_name,pipeline_file.md5,pipeline_file.local_file,pipeline_file.created_at,pipeline_file.updated_at,user.realName").Joins("left join user on pipeline_file.uid = user.id").Offset(offset).Limit(limit).Order("id desc").Find(&data).Error
	if err != nil {
		//log.Fatalln(err)
	}
	totalPages := uint(math.Ceil(float64(totalCount) / float64(limit)))
	return data, totalCount, totalPages
}

func (m *PipelineFile) ListAll(uid uint) []PipelineFileList {
	var data = []PipelineFileList{}
	db := libs.DB
	db = m.adminCheck(uid, db)
	_ = db.Table("pipeline_file").Select("pipeline_file.id,pipeline_file.uid,pipeline_file.pid,pipeline_file.short_name,pipeline_file.md5,pipeline_file.local_file,pipeline_file.created_at,pipeline_file.updated_at,user.realName").Order("id desc").Joins("left join user on pipeline_file.uid = user.id").Find(&data).Error
	return data
}

func (m *PipelineFile) Create() error {
	var pipelineFile PipelineFile
	if err := util.Validate(m); err != nil {
		return err
	}
	db := libs.DB
	if db.Where("`md5` = ? and uid = ? ", m.Md5, m.Uid).First(&pipelineFile).RecordNotFound() {
		return db.Create(m).Error
	}
	m.ID = pipelineFile.ID
	return db.Save(m).Error
}

func (m *PipelineFile) Del(id uint) error {
	db := libs.DB
	return db.Where("id = ?", id).Delete(m).Error
}

func (m *PipelineFile) Detail(id uint) (pipelineFile PipelineFile, err error) {
	db := libs.DB
	err = db.Where("id = ?", id).First(&pipelineFile).Error
	return
}

func (m *PipelineFile) Last() (PipelineFile, error) {
	var pipelineFile PipelineFile
	db := libs.DB
	db.Last(&pipelineFile)
	return pipelineFile, nil
}

func (m *PipelineFile) adminCheck(uid uint, db *gorm.DB) *gorm.DB {
	roleUser, _ := NewUserRoleUser().GetUserRole(int(uid))
	if roleUser.RoleId != 1 {
		db = db.Where("uid = ?", uid)
	}
	return db
}
