package ymlgo

import (
	"devOpsApi/config"
	"gopkg.in/yaml.v2"
	"io/ioutil"
)

type DevelopCate struct {
	Name string `yaml:"name"`
	Tag  string `yaml:"tag"`
}

type ConfigSysBase struct {
	ServerPort            string `yaml:"ServerPort"`
	JwtSecret             string `yaml:"JwtSecret"`
	UploadSize            uint   `yaml:"UploadSize"`
	JenkinsServer         string `yaml:"JenkinsServer"`
	JenkinsAuthUser       string `yaml:"JenkinsAuthUser"`
	JenkinsAuthPwd        string `yaml:"JenkinsAuthPwd"`
	GitLabServer          string `yaml:"GitLabServer"`
	GitLabRootAccessToken string `yaml:"GitLabRootAccessToken"`
	Domain                string `yaml:"Domain"`
	PrometheusServer      string `yaml:"PrometheusServer"`
	OKDServer             string `yaml:"OKDServer"`
	OKDUsername           string `yaml:"OKDUsername"`
	OKDPassword           string `yaml:"OKDPassword"`
	JiraServer            string `yaml:"JiraServer"`
	ZentaoServer          string `yaml:"ZentaoServer"`
	ZentaoRootAccount     string `yaml:"ZentaoRootAccount"`
	ZentaoRootPassword    string `yaml:"ZentaoRootPassword"`
	SonarHost             string `yaml:"SonarHost"`
	SonarToken            string `yaml:"SonarToken"`
}

func NewConfigSysBase() *ConfigSysBase {
	data, _ := ioutil.ReadFile(config.ConfigSysBasePath)
	t := ConfigSysBase{}
	//把yaml形式的字符串解析成struct类型
	yaml.Unmarshal(data, &t)
	return &t
}
