package ymlgo

import (
	"devOpsApi/config"
	"gopkg.in/yaml.v2"
	"io/ioutil"
)

type ConfigMysql struct {
	MysqlHost         string `yaml:"mysqlHost"`
	MysqlPort         string `yaml:"mysqlPort"`
	MysqlUser         string `yaml:"mysqlUser"`
	MysqlPwd          string `yaml:"mysqlPwd"`
	MysqlDatabase     string `yaml:"mysqlDatabase"`
	MysqlCharset      string `yaml:"mysqlCharset"`
	MysqlSqlLog       bool   `yaml:"mysqlSqlLog"`
	MysqlMaxIdleConns int    `yaml:"mysqlMaxIdleConns"`
	MysqlOpenConns    int    `yaml:"mysqlOpenConns"`
}

//获取Mysql配置
func NewMysqlConfig() *ConfigMysql {
	data, _ := ioutil.ReadFile(config.ConfigMysqlPath)
	t := ConfigMysql{}

	//把yaml形式的字符串解析成struct类型
	yaml.Unmarshal(data, &t)
	return &t
}
