package config

var ConfigEmailPath = "./config/yml/email.yml"
var ConfigMysqlPath = "./config/yml/mysql.yml"
var ConfigSysBasePath = "./config/yml/sysbase.yml"
var ConfigJavaJenkinsPath = "./config/jenkins/java.xml"
var ConfigNodeJenkinsPath = "./config/jenkins/node.xml"
