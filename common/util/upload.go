package util

import (
	"devOpsApi/config/ymlgo"
	"github.com/kataras/iris"
	"io"
	"os"
)

func UploadPipeLineFile(key string, _path string, Ctx iris.Context) (bool, string) {
	config := ymlgo.NewConfigSysBase()
	file, info, err := Ctx.FormFile(key)
	filePath := ""
	if err != nil {
		return false, "Error while uploading: <b>" + err.Error() + "</b>"
	}
	var minSize int64 = -1
	if info.Size > minSize {
		if info.Size > int64(config.UploadSize)*1024*1024 {
			return false, "Error while uploading: UploadSize ToMax"
		}
		basePath := "./uploads/pipelineFile/" + _path + "/"
		filePath = basePath + info.Filename
		if err := PathExists(basePath); err != nil {
			err := os.MkdirAll(basePath, 755)
			if err != nil {
				return false, "MdDir error: " + err.Error()
			}
		}
		out, err := os.OpenFile(filePath, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0777)
		if err != nil {
			return false, "Error while uploading: <b>" + err.Error() + "</b>"
		}
		defer out.Close()
		if _, err := io.Copy(out, file); err != nil {
			return false, err.Error()
		}
		//filePath = filePath[1:]
	}
	defer file.Close()
	return true, filePath
}
