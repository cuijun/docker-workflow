package util

import (
	"errors"
	"github.com/go-playground/locales/zh"
	"github.com/go-playground/universal-translator"
	"gopkg.in/go-playground/validator.v9"
	zh_translations "gopkg.in/go-playground/validator.v9/translations/zh"
	"regexp"
)

/*
	以下是当前内置验证器的列表：
	required    //这将验证该值不是数据类型的默认零值。数字不为０，字符串不为 " ", slices, maps, pointers, interfaces, channels and functions 不为 nil
	isdefault    //这验证了该值是默认值，几乎与所需值相反。
	len=10    //对于数字，长度将确保该值等于给定的参数。对于字符串，它会检查字符串长度是否与字符数完全相同。对于切片，数组和map，验证元素个数。
	max=10    //对于数字，max将确保该值小于或等于给定的参数。对于字符串，它会检查字符串长度是否最多为该字符数。对于切片，数组和map，验证元素个数。
	min=10
	eq=10    //对于字符串和数字，eq将确保该值等于给定的参数。对于切片，数组和map，验证元素个数。
	ne=10    //和eq相反
	oneof=red green (oneof=5 7 9)    //对于字符串，整数和uint，oneof将确保该值是参数中的值之一。参数应该是由空格分隔的值列表。值可以是字符串或数字。
	gt=10    //对于数字，这将确保该值大于给定的参数。对于字符串，它会检查字符串长度是否大于该字符数。对于切片，数组和map，它会验证元素个数。
	gt    //对于time.Time确保时间值大于time.Now.UTC（）
	gte=10    //大于等于
	gte    //对于time.Time确保时间值大于或等于time.Now.UTC（）
	lt=10    //小于
	lt    //对于time.Time确保时间值小于time.Now.UTC（）
	lte=10    //小于等于
	lte    //对于time.Time确保时间值小于等于time.Now.UTC（）

	unique    //对于数组和切片，unique将确保没有重复项。对于map，unique将确保没有重复值。
	alpha    //这将验证字符串值是否仅包含ASCII字母字符
	alphanum    //这将验证字符串值是否仅包含ASCII字母数字字符
	alphaunicode    //这将验证字符串值是否仅包含unicode字符
	alphanumunicode    //这将验证字符串值是否仅包含unicode字母数字字符
	numeric    //这将验证字符串值是否包含基本数值。基本排除指数等...对于整数或浮点数，它返回true。
	hexadecimal    //这将验证字符串值是否包含有效的十六进制
	hexcolor    //这验证字符串值包含有效的十六进制颜色，包括＃标签（＃）
	rgb    //这将验证字符串值是否包含有效的rgb颜色
	rgba    //这将验证字符串值是否包含有效的rgba颜色
	hsl    //这将验证字符串值是否包含有效的hsl颜色
	hsla    //这将验证字符串值是否包含有效的hsla颜色
	email    //这验证字符串值包含有效的电子邮件这可能不符合任何rfc标准的所有可能性，但任何电子邮件提供商都不接受所有可能性
	file    //这将验证字符串值是否包含有效的文件路径，并且该文件存在于计算机上。这是使用os.Stat完成的，它是一个独立于平台的函数。
	url    //这会验证字符串值是否包含有效的url这将接受golang请求uri接受的任何url，但必须包含一个模式，例如http：//或rtmp：//
	uri    //这验证了字符串值包含有效的uri。这将接受uri接受的golang请求的任何uri
	base64    //这将验证字符串值是否包含有效的base64值。虽然空字符串是有效的base64，但这会将空字符串报告为错误，如果您希望接受空字符串作为有效字符，则可以将此字符串与omitempty标记一起使用。
	base64url    //这会根据RFC4648规范验证字符串值是否包含有效的base64 URL安全值。尽管空字符串是有效的base64 URL安全值，但这会将空字符串报告为错误，如果您希望接受空字符串作为有效字符，则可以将此字符串与omitempty标记一起使用。
	btc_addr    //这将验证字符串值是否包含有效的比特币地址。检查字符串的格式以确保它匹配P2PKH，P2SH三种格式之一并执行校验和验证
	btc_addr_bech32    //这验证了字符串值包含bip-0173定义的有效比特币Bech32地址（https://github.com/bitcoin/bips/blob/master/bip-0173.mediawiki）特别感谢Pieter Wuille提供的参考实现。
	eth_addr    //这将验证字符串值是否包含有效的以太坊地址。检查字符串的格式以确保它符合标准的以太坊地址格式完全验证被https://github.com/golang/crypto/pull/28阻止
	contains=@    //这将验证字符串值是否包含子字符串值
	containsany=!@#?    //这将验证字符串值是否包含子字符串值中的任何Unicode code points。
	containsrune=@    //这将验证字符串值是否包含提供的符文值。
	excludes=@    //这验证字符串值不包含子字符串值。
	excludesall=!@#?    //这将验证字符串值在子字符串值中是否包含任何Unicode code points。
	excludesrune=@    //这将验证字符串值是否包含提供的符文值。

	isbn    //这将验证字符串值是否包含有效的isbn10或isbn13值。
	isbn10    //这将验证字符串值是否包含有效的isbn10值。
	isbn13    //这将验证字符串值是否包含有效的isbn13值。
	uuid    //这将验证字符串值是否包含有效的UUID。
	uuid3    //这将验证字符串值是否包含有效的版本3 UUID。
	uuid4    //这将验证字符串值是否包含有效的版本4 UUID。
	uuid5    //这将验证字符串值是否包含有效的版本5 UUID。

	ascii    //这将验证字符串值是否仅包含ASCII字符。注意：如果字符串为空，则验证为true
	printascii    //这将验证字符串值是否仅包含可打印的ASCII字符。注意：如果字符串为空，则验证为true。
	multibyte    //这将验证字符串值是否包含一个或多个多字节字符。注意：如果字符串为空，则验证为true
	datauri    //这将验证字符串值是否包含有效的DataURI。注意：这也将验证数据部分是否有效base64
	latitude    //这将验证字符串值是否包含有效的纬度。
	longitude    //这将验证字符串值是否包含有效经度。
	ssn    //这将验证字符串值是否包含有效的美国社会安全号码。
	ip    //这将验证字符串值是否包含有效的IP地址
	ipv4    //这将验证字符串值是否包含有效的v4 IP地址
	ipv6    //这将验证字符串值是否包含有效的v6 IP地址
	cidr    //这将验证字符串值是否包含有效的CIDR地址
	cidrv4    //这将验证字符串值是否包含有效的v4 CIDR地址
	cidrv5    //这将验证字符串值是否包含有效的v5 CIDR地址
	tcp_addr    //这将验证字符串值是否包含有效的可解析TCP地址
	tcp4_addr    //这将验证字符串值是否包含有效的可解析v4 TCP地址
	tcp6_addr    //这将验证字符串值是否包含有效的可解析v6 TCP地址
	udp_addr    //这将验证字符串值是否包含有效的可解析UDP地址
	udp4_addr    //这将验证字符串值是否包含有效的可解析v4 UDP地址
	udp6_addr    //这将验证字符串值是否包含有效的可解析v6 UDP地址
	ip_addr    //这将验证字符串值是否包含有效的可解析IP地址
	ip4_addr    //这将验证字符串值是否包含有效的可解析v4 IP地址
	ip6_addr    //这将验证字符串值是否包含有效的可解析v6 IP地址
	unix_addr    //这将验证字符串值是否包含有效的Unix地址

	mac    //这将验证字符串值是否包含有效的MAC地址
	//注意：有关可接受的格式和类型，请参阅Go的ParseMAC: http://golang.org/src/net/mac.go?s=866:918#L29
	hostname    //根据RFC 952 https://tools.ietf.org/html/rfc952验证字符串值是否为有效主机名
	hostname_rfc1123 or if you want to continue to use 'hostname' in your tags, create an alias    //根据RFC 1123 https://tools.ietf.org/html/rfc1123验证字符串值是否为有效主机名
	fqdn    //这将验证字符串值是否包含有效的FQDN (完全合格的有效域名)，Full Qualified Domain Name (FQDN)
	html    //这将验证字符串值是否为HTML元素标记，包括https://developer.mozilla.org/en-US/docs/Web/HTML/Element中描述的标记。
	html_encoded    //这将验证字符串值是十进制或十六进制格式的正确字符引用
	url_encoded    //这验证了根据https://tools.ietf.org/html/rfc3986#section-2.1对字符串值进行了百分比编码（URL编码）
*/

var (
	uni      *ut.UniversalTranslator
	validate *validator.Validate
)

//结构体验证
func Validate(s interface{}) error {
	zh := zh.New()
	trans, _ := ut.New(zh, zh).GetTranslator("zh")
	validate = validator.New()
	_ = validate.RegisterValidation("phone", ValidatePhone) //注册验证字段和字段验证的功能
	_ = zh_translations.RegisterDefaultTranslations(validate, trans)
	err := validate.Struct(s)
	errStr := ""
	if err != nil {
		errs := err.(validator.ValidationErrors)
		for _, e := range errs {
			errStr += e.Translate(trans) + "\n"
		}
		return errors.New(errStr)
	}
	return nil
}

//验证字段
//util.ValidateField("12345678","required,email")
func ValidateField(s interface{}, tag string) error {
	zh := zh.New()
	trans, _ := ut.New(zh, zh).GetTranslator("zh")
	validate = validator.New()
	_ = validate.RegisterValidation("phone", ValidatePhone) //注册验证字段和字段验证的功能
	_ = zh_translations.RegisterDefaultTranslations(validate, trans)
	err := validate.Var(s, tag)
	errStr := ""
	if err != nil {
		errs := err.(validator.ValidationErrors)
		for _, e := range errs {
			errStr += e.Translate(trans) + "\n"
		}
		return errors.New(errStr)
	}
	return nil
}

/*
两个字段比较
eqfield 相等
nefield 不等
gtfield 大于
gtefield 大于或等于
ltfield 小于
ltefield 小于或等于
*/
func ValidateFieldWith(s1, s2 interface{}, tag string) error {
	zh := zh.New()
	trans, _ := ut.New(zh, zh).GetTranslator("zh")
	validate = validator.New()
	_ = zh_translations.RegisterDefaultTranslations(validate, trans)
	err := validate.VarWithValue(s1, s2, tag)
	errStr := ""
	if err != nil {
		errs := err.(validator.ValidationErrors)
		for _, e := range errs {
			errStr += e.Translate(trans) + "\n"
		}
		return errors.New(errStr)
	}
	return nil
}

//自定义验证 手机号码
func ValidatePhone(f1 validator.FieldLevel) bool {
	regular := "^((13[0-9])|(14[5,7])|(15[0-3,5-9])|(17[0,3,5-8])|(18[0-9])|166|198|199|(147))\\d{8}$"
	reg := regexp.MustCompile(regular)
	return reg.MatchString(f1.Field().String())
}
