package util

import (
	"fmt"
	"sync"
	"testing"
)

func TestPathParam2Val(t *testing.T) {
	m := map[string]string{
		"id":   "22",
		"name": "feb",
	}
	target, err := PathParam2Val(`http://10.13.0.167:8000/interface/v1/:id/:name`, m)
	if err != nil {
		fmt.Println(`err: `, err.Error())
		return
	}
	fmt.Println(target)
}

func TestMap(t *testing.T) {
	m := sync.Map{}
	m.Store("name1", "feb")
	m.Store("name2", "feb1")

	m.Range(func(key, value interface{}) bool {
		t.Logf("k:%s,val:%s", key, value)
		return true
	})

	t.Log(m.Load("name"))
}
