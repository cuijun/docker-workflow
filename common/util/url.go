package util

import (
	"net/url"
	"strings"
)

//把路径参数放入到请求地址里
func PathParam2Val(uri string, param map[string]string) (target string, err error) {
	if uri == "" || param == nil {
		return
	}
	u, err := url.Parse(uri)
	if err != nil {
		return
	}
	str := ``
	path := u.EscapedPath()
	arr := strings.Split(path, `/`)
	for _, s := range arr {
		if s != `` {
			if strings.Contains(s, `:`) {
				s = param[s[1:]]
			}
			str += `/` + s
		}
	}
	if str != `` {
		target = u.Scheme + `://` + u.Host + str
	}
	return
}
