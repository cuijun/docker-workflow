package util

const (
	numberUniqueThreshold = 1024
	stringUniqueThreshold = 256
)

func UintsUnique(a []uint) []uint {
	l := len(a)

	if l <= 1 {
		return a
	}

	r := make([]uint, 0, l)

	// remove duplicates with loop
	if l < numberUniqueThreshold {
		for _, v := range a {
			exist := false

			for _, u := range r {
				if v == u {
					exist = true
					break
				}
			}

			if !exist {
				r = append(r, v)
			}
		}

		return r
	}

	// remove duplicates with map
	m := make(map[uint]byte, l)

	for _, v := range a {
		if _, ok := m[v]; !ok {
			m[v] = 0
			r = append(r, v)
		}
	}

	return r
}

func IntsUnique(a []int) []int {
	l := len(a)

	if l <= 1 {
		return a
	}

	r := make([]int, 0, l)

	// remove duplicates with loop
	if l < numberUniqueThreshold {
		for _, v := range a {
			exist := false

			for _, u := range r {
				if v == u {
					exist = true
					break
				}
			}

			if !exist {
				r = append(r, v)
			}
		}

		return r
	}

	// remove duplicates with map
	m := make(map[int]byte, l)

	for _, v := range a {
		if _, ok := m[v]; !ok {
			m[v] = 0
			r = append(r, v)
		}
	}

	return r
}
