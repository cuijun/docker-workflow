# 云上研发工作流

#### 介绍
容器化云上研发工作流，支持编译打包各种语言代码，发布至各种服务，主机，镜像构建，镜像仓库构建、部署，推送git仓库

#### 演示图
![首页面](demo.gif "首页面")



#### 安装教程

1.  安装docker
2.  导入sql文件
3.  构建代码
3.  修改config/yml配置
4.  执行二进制文件

#### 使用说明

1.  默认账号密码 admin admin888
2.  内部仓库使用gitlab
3.  前端代码地址：https://gitee.com/cuijun/docker-workflow.git-front

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技
联系wx：js_10000
