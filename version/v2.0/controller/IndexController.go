package controllers

import (
	v1 "devOpsApi/version/v1.0/controller"
	"github.com/kataras/iris"
)

//v2.0 可以集成 v1 控制器方法
type IndexController struct {
	v1.IndexController
}

func (c *IndexController) GetApiVersion() {
	c.OutDataJson("", iris.StatusOK, "v2.1")
}
