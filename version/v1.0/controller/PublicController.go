package controllers

//不需要RBAC 验证
import (
	"devOpsApi/base"
	"devOpsApi/middleware"
	"devOpsApi/model"
	"github.com/kataras/iris"
)

type PublicController struct {
	base.BController
}

//PanelGroup
func (c *PublicController) GetPanelGroup() {
	uid := int(middleware.GetTokenUId(c.Ctx))
	gitModel, _ := model.NewUserGitlab(uid)
	PipelineModel := model.NewPipeline()
	list := gitModel.AllRepoList()
	Pipeline := PipelineModel.ListAll(uint(uid))
	var successNum, errNum uint = 0, 0
	for _, value := range Pipeline {
		successNum += value.SuccessNum
		errNum += value.ErrorNum
	}
	res := iris.Map{
		"gitNum":      len(list),
		"pipelineNum": len(Pipeline),
		"successNum":  successNum,
		"errNum":      errNum,
	}
	c.OutDataJson(res, iris.StatusOK, "success")
	return
}

// test
func (this *PublicController) GetList() {
	userId := middleware.GetTokenUserId(this.Ctx)
	this.OutDataJson(userId, iris.StatusOK, "success")
	return
}
