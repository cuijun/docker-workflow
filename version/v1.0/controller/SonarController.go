package controllers

import (
	"devOpsApi/config/ymlgo"
	"devOpsApi/middleware"
	"github.com/kataras/iris"

	"devOpsApi/base"
	v1Model "devOpsApi/version/v1.0/model"
)

type SonarController struct {
	base.BController
}

//获取所有的质量阀值
func (c *SonarController) GetPublicQgUserAll() {
	Sonar, err := v1Model.NewSonar()
	if err != nil {
		c.OutDataJson("", iris.StatusBadRequest, "Sonar异常! :"+err.Error())
		return
	}
	list, err := Sonar.UserAll(uint(middleware.GetTokenUId(c.Ctx)))
	if err != nil {
		c.OutDataJson("", iris.StatusBadRequest, "Sonar异常! :"+err.Error())
		return
	}
	c.OutDataJson(list, iris.StatusOK, "success")
}

//创建质量阀
func (c *SonarController) PostQgCreate() {
	name := c.Ctx.PostValueDefault("name", "")
	if name == "" {
		c.OutDataJson("", iris.StatusBadRequest, "质量阀名称不能为空")
		return
	}
	Sonar, err := v1Model.NewSonar()
	if err != nil {
		c.OutDataJson("", iris.StatusBadRequest, "Sonar异常! :"+err.Error())
		return
	}
	qualityGate, err := Sonar.Create(uint(middleware.GetTokenUId(c.Ctx)), name)
	if err != nil {
		c.OutDataJson("", iris.StatusBadRequest, err.Error())
		return
	}
	c.OutDataJson(qualityGate, iris.StatusOK, "success")
}

//修改质量阀
func (c *SonarController) PostQgRename() {
	id := c.Ctx.PostValueDefault("id", "")
	name := c.Ctx.PostValueDefault("name", "")
	if name == "" || id == "" {
		c.OutDataJson("", iris.StatusBadRequest, "质量阀名称不能为空")
		return
	}
	Sonar, err := v1Model.NewSonar()
	if err != nil {
		c.OutDataJson("", iris.StatusBadRequest, "Sonar异常! :"+err.Error())
		return
	}
	qualityGate, err := Sonar.Update(uint(middleware.GetTokenUId(c.Ctx)), id, name)
	if err != nil {
		c.OutDataJson("", iris.StatusBadRequest, err.Error())
		return
	}
	c.OutDataJson(qualityGate, iris.StatusOK, "success")
}

//删除质量阀
func (c *SonarController) PostQgDel() {
	name := c.Ctx.PostValueDefault("name", "")
	if name == "" {
		c.OutDataJson("", iris.StatusBadRequest, "质量阀名称不能为空")
		return
	}
	Sonar, err := v1Model.NewSonar()
	if err != nil {
		c.OutDataJson("", iris.StatusBadRequest, "Sonar异常! :"+err.Error())
		return
	}
	err = Sonar.Del(uint(middleware.GetTokenUId(c.Ctx)), name)
	if err != nil {
		c.OutDataJson("", iris.StatusBadRequest, err.Error())
		return
	}
	c.OutDataJson("", iris.StatusOK, "success")
}

//质量阀详情
func (c *SonarController) GetQgShow() {
	name := c.Ctx.URLParamDefault("name", "")
	if name == "" {
		c.OutDataJson("", iris.StatusBadRequest, "质量阀名称不能为空")
		return
	}
	Sonar, err := v1Model.NewSonar()
	if err != nil {
		c.OutDataJson("", iris.StatusBadRequest, "Sonar异常! :"+err.Error())
		return
	}
	qualityGate, err := Sonar.Detail(uint(middleware.GetTokenUId(c.Ctx)), name)
	if err != nil {
		c.OutDataJson("", iris.StatusBadRequest, err.Error())
		return
	}
	c.OutDataJson(qualityGate, iris.StatusOK, "success")
}

//质量阀条件列表
func (c *SonarController) GetQgConditionList() {
	Sonar, err := v1Model.NewSonar()
	if err != nil {
		c.OutDataJson("", iris.StatusBadRequest, "Sonar异常! :"+err.Error())
		return
	}
	list := Sonar.ConditionList()
	c.OutDataJson(list, iris.StatusOK, "success")
}

//新增质量阀条件
func (c *SonarController) PostQgCreateCondition() {
	Sonar, err := v1Model.NewSonar()
	if err != nil {
		c.OutDataJson("", iris.StatusBadRequest, "Sonar异常! :"+err.Error())
		return
	}
	errinfo := c.Ctx.PostValueDefault("error", "") //指标值
	gateId := c.Ctx.PostValueDefault("gateId", "") //质量阀id
	metric := c.Ctx.PostValueDefault("metric", "") //指标名称
	op := c.Ctx.PostValueDefault("op", "")         //大于小于
	if errinfo == "" || gateId == "" || metric == "" || op == "" {
		c.OutDataJson("", iris.StatusBadRequest, "参数丢失")
		return
	}

	qualityGatesCondition, err := Sonar.CreateCondition(errinfo, gateId, metric, op)
	if err != nil {
		c.OutDataJson("", iris.StatusBadRequest, err.Error())
		return
	}
	c.OutDataJson(qualityGatesCondition, iris.StatusOK, "success")
}

//删除质量阀条件
func (c *SonarController) PostQgDelCondition() {
	Sonar, err := v1Model.NewSonar()
	if err != nil {
		c.OutDataJson("", iris.StatusBadRequest, "Sonar异常! :"+err.Error())
		return
	}
	id := c.Ctx.PostValueDefault("id", "") //质量阀条件id
	if id == "" {
		c.OutDataJson("", iris.StatusBadRequest, "参数丢失")
		return
	}
	err = Sonar.DeleteCondition(id)
	if err != nil {
		c.OutDataJson("", iris.StatusBadRequest, "Sonar异常! :"+err.Error())
		return
	}
	c.OutDataJson("", iris.StatusOK, "success")
}

//修改质量阀条件
func (c *SonarController) PostQgUpdateCondition() {
	Sonar, err := v1Model.NewSonar()
	if err != nil {
		c.OutDataJson("", iris.StatusBadRequest, "Sonar异常! :"+err.Error())
		return
	}
	errinfo := c.Ctx.PostValueDefault("error", "") //指标值
	id := c.Ctx.PostValueDefault("id", "")         //质量阀条件id
	metric := c.Ctx.PostValueDefault("metric", "") //指标名称
	op := c.Ctx.PostValueDefault("op", "")         //大于小于
	if errinfo == "" || id == "" || metric == "" || op == "" {
		c.OutDataJson("", iris.StatusBadRequest, "参数丢失")
		return
	}

	qualityGatesCondition, err := Sonar.UpdateCondition(errinfo, metric, op, id)
	if err != nil {
		c.OutDataJson("", iris.StatusBadRequest, err.Error())
		return
	}
	c.OutDataJson(qualityGatesCondition, iris.StatusOK, "success")
}

//获取质量报告地址
func (c *SonarController) GetProjectReport() {
	name := c.Ctx.URLParamDefault("name", "")
	if name == "" {
		c.OutDataJson("", iris.StatusBadRequest, "name参数丢失")
		return
	}
	Sonar, err := v1Model.NewSonar()
	if err != nil {
		c.OutDataJson("", iris.StatusBadRequest, err.Error())
		return
	}
	detail, err := Sonar.ProjectDetail(name)
	if err != nil {
		c.OutDataJson("", iris.StatusBadRequest, err.Error())
		return
	}

	if len(detail.Components) == 0 {
		c.OutDataJson("", iris.StatusBadRequest, "未找到质量报告项目,请确认该项目是否使用了质量测试模块！")
		return
	}

	config := ymlgo.NewConfigSysBase()
	c.OutDataJson(config.SonarHost+"/dashboard?id="+name, iris.StatusOK, "success")
}
