package route

import (
	"github.com/kataras/iris"
	"github.com/kataras/iris/mvc"

	"devOpsApi/middleware"
	v1 "devOpsApi/version/v1.0/controller"
	v20 "devOpsApi/version/v2.0/controller"
)

/*
 使用middleware.JwtHandlerUser中间件则为需要验证的方法，不使用则无需验证
*/

func Routes(app *iris.Application) {
	// v1.0版本
	const V1Base = "/v1"
	mvc.New(app.Party(V1Base+"/", middleware.CacheServe)).Handle(new(v1.IndexController))
	mvc.New(app.Party(V1Base+"/public", middleware.Auth)).Handle(new(v1.PublicController))
	mvc.New(app.Party(V1Base+"/pipeline", middleware.Auth, middleware.RBAC)).Handle(new(v1.PipelineController))
	mvc.New(app.Party(V1Base+"/sonar", middleware.Auth, middleware.RBAC)).Handle(new(v1.SonarController))
	mvc.New(app.Party(V1Base+"/git", middleware.Auth, middleware.RBAC)).Handle(new(v1.GitController))
	mvc.New(app.Party(V1Base+"/user", middleware.Auth, middleware.RBAC)).Handle(new(v1.UserController))
	mvc.New(app.Party(V1Base+"/setting", middleware.Auth, middleware.RBAC)).Handle(new(v1.SettingController))

	// v2.0版本
	const V2_1Base = "/v2.0"
	mvc.New(app.Party(V2_1Base + "/")).Handle(new(v20.IndexController))
}
